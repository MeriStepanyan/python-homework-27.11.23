# 1. Write a Python program that takes a sentence as input and returns the longest word along with its length. 
# Use a dictionary to store word lengths.
def dict_keywith_max_value(string):
    keys_list=string.split()
    
    values_list=[len(i)  for i in keys_list]
    
    dict1={k:v for k,v in zip(keys_list,values_list)}
    
    max_val=0
    for i in dict1:
        if dict1[i]>max_val:
            max_val=dict1[i]
    key = {i for i in dict1 if dict1[i]==max_val} 
    return key,max_val
string="I am very happy today"    
print("The longest words with their lengths. ",dict_keywith_max_value(string))        
    

def dict_keywith_max_value1(string):
    keys_list=string.split()
    values_list=[len(i)  for i in keys_list]
    dict1={k:v for k,v in zip(keys_list,values_list)}
    key = {i for i in dict1 if dict1[i]==max(values_list)} 
    return key, max(values_list) 
    
string="I am very happy today"    
print("The longest words with their lengths. ",dict_keywith_max_value1(string)) 

# 2. Write a Python program that takes a dictionary where keys are student names and values are lists of grades. 
# Calculate and return the average grade for each student.
import statistics
dict2={"Ann":[85,60,93,70], "Adam":[60,55,70,62],"Betty":[78,70,80,72]}
dict3={k:statistics.mean(v) for k,v in dict2.items() }
print("The dictionary with average grade for each student",dict3)

# 3. Write a Python program that takes two lists containing numbers, where one list is a subset of the other. 
# Find the missing element in the second list and print it.
list2=[1,3,5,-2,7,9]
list3=[5,1,7]
set1=set(list2)
set2=set(list3)
print("The missing elements are: ",set1.difference(set2))

# 4. Write a Python program that takes a dictionary with string keys in camel case and transforms them 
# into snake case keys.
def camelCaseTo_snake_case(dict3):
    keys=dict3.keys()
    keys=list(keys)
    for k in range(len(keys)):
        for i in range(len(keys[k])):
            if not(keys[k][i].isupper()):
                continue
            else:
                keys[k]=keys[k][:i]+"_"+keys[k][i:].lower()
    keys=list(keys)
    values=list(dict3.values())
    dict_snake_case={k:v for k,v in zip(keys,values)}
    return dict_snake_case
dictCamelCase={"camelCase":1,"snakeCase":2}
print(camelCaseTo_snake_case(dictCamelCase))

#5. Create a quiz game with multiple-choice questions. Use a dictionary to store questions and correct answers. 
import time
def quiz_game(dict_c,dict_m_c):

    name=input("Please enter your name: ")
    success=0
    failure=0
    print(f"Hello {name}, Complete each question by selecting the correct answer. You can only select one answer for each question.\nYou will be notified of your results after submitting your answers.\n")
    time.sleep(2)
    for i in dict_m_c:
        print(i)
        for j in range(len(dict_m_c[i])):
            print(dict_m_c[i][j])
        answer=input("Please enter your answer: ")
        if answer==dict_c[i]:
            success+=1
            if(success>=4):
                print("\nYour answer is correct, well done!\n")
            else:
                print("\nYour answer is correct!\n")
        else:
            print("\nYour answer isn't correct!\n")
            failure+=1
        time.sleep(2)     
    if(success==6):
        print(f"Great job {name}, you answered all the questions correctly, congratulations !!!")
    if(success>=4 and success<=5):
        print(f"Good job {name}, you answered {success} questions correctly, congratulations !")
    if(success>=2 and success<=3):
        print(f"Not so bad {name}, you answered {success} questions correctly")
    if(success>=0 and success<=1):
        print(f"You have bad results {name}, you answered {success} question correctly")    
    
dict_correct_answer={"Who was the Ancient Greek God of the Sun?": "Apollo",
"Who has won the most total Academy Awards?": "Walt Disney",
"How many minutes are in a full week?" : "10080",
"What car manufacturer had the highest revenue in 2020?" :"Volkswagen",
"How many elements are in the periodic table?" :"118",
"What company was originally called 'Cadabra?'":"Amazon"}  
dict_mul_choises={"Who was the Ancient Greek God of the Sun?": ["1.Artemis","2.Poseidon","3.Apollo","4.Athena"],
"Who has won the most total Academy Awards?": ["1.Dennis Muren","2.Walt Disney","3.Katharine Hepburn","4.Katharine Hepburn"],
"How many minutes are in a full week?" : ["1. 10080","2. 604800","3. 43800","4. 11340"],
"What car manufacturer had the highest revenue in 2020?" :["1.Mercedes-Benz","2.Tesla","3.Volkswagen","4.Honda"],
"How many elements are in the periodic table?" :["1. 118","2. 99","3. 100","4. 160"],
"What company was originally called 'Cadabra?'":["1.Amazon","2.Aliexpress","3.eBay","4.Target"]}

quiz_game(dict_correct_answer,dict_mul_choises)   

#6. Write a program that randomly assigns Secret Santa pairs among a group of participants.
#  Use a dictionary to represent the pairings.
import random
list4=["Ann","Michael","Jane","Karen","Sebastian","Adam"]
k_set=set()
while(len(k_set)!=3):
    k_set.add(random.choice(list4))

k_list=list(k_set)
v_list=[i for i in list4 if i not in k_list] 

k_set=set(k_list)
v_set=set(v_list)
dict_pairs={k:v for k,v in zip(k_set,v_set)}
print("The Secret Santa pairs:",dict_pairs)

# 7. Write a Python program that takes a string containing parentheses and checks if the parentheses are balanced.
#  Use a dictionary to map opening and closing parentheses․
def is_balanced(string):
    s_list=list(string)
    open_brackets=['{','[','(']
    close_brackets=['}',']',')']
    bool_b=False
    for i in s_list:
        if i in open_brackets:
            c_index=open_brackets.index(i)
            if(s_list.count(i)==s_list.count(close_brackets[c_index])):
                bool_b=True
                continue
            else:
                bool_b=False
                return "Unbalanced"
                break
    if(bool_b):
        return "Balanced"
string="(){[()]}"
print(f"The '{string}' is:",is_balanced(string))
